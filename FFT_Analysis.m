%Delete workspace's data
clear;
%Read the data from excel into a local array.
DataFile = {'91845_NacelleHits.xls','92509_LessThan9ms.xls','092902_.xls','093452_.xls','93626_VarSpeed.xls','93728DownSpeed.xls','93835_9msTowerMoved.xls','094200_.xls','094316_Idling.xls','094528_.xls','094746_.xls','95239SpeedUpto9ms.xls','095616_Downspeed.xls'};   
Cells = {'A3','A4','A5','A6','A7','A8','A9','A10','A11','A12','A13','A14','A15',};

for z = [1:13]

    FolderName = strrep(DataFile{z}, '.xls','')
    DataArray1= xlsread(DataFile{z});
    %Determine the size of the array
    Size= size(DataArray1);
    Rows= Size(1,1);
    Columns = Size(1,2);
    %Cell with color identifiers
    C = {'b','k','r'};
    %Cell with titles' strings
    TitleLabel = {'Espectral response X Axis','Espectral response Y Axis','Espectral response Z Axis'};
    FileName = {'X','Y','Z'};
    %Save the axes' data into new variables
    for i = [1:Rows] 
        %Set delta time in seconds resolution
        dt(i) =(DataArray1(i,1)/1000000);
        XAxis(i) =(DataArray1(i,2));
        YAxis(i) =(DataArray1(i,3));
        ZAxis(i) =(DataArray1(i,4));
        i = i+1;
        end
    %Calculate sampling frequency using the mean value of the sampling period
    fs = 1/(mean(dt));
    % Data length
    m = length(XAxis);          
    % Use Hanning window on the input data
    XAxis = XAxis.*(transpose(hanning(m)));
    % FFT
    X = fft(XAxis,m);           
    %The first and second element of the series contain data with low
    %frequency and relatively high magnitude. This can be the offsets of the
    %sensor. The next two lines remove this values.
    X(1)=0;
    X(2)=0;
    freq = (1:m)*(fs/(m)); % Frequency range
    powerX = 2*abs(X)/m;   % Power of the DFT

    YAxis = YAxis.*(transpose(hanning(m)));
    Y = fft(YAxis,m);           
    Y(1)=0;
    Y(2)=0;
    powerY = 2*abs(Y)/m;  

    ZAxis = ZAxis.*(transpose(hanning(m)));
    Z = fft(ZAxis,m);           
    Z(1)=0;
    Z(2)=0;
    powerZ = 2*abs(Z)/m;   

    Power={powerX,powerY,powerZ}; %Cell that contains the 3 power vectors.
    %Plot separately the 3 axes
    for i = [1:3]
        %Create new window
        figure;
        %Graph the first half of the data set (second half is redundant)
        plot(freq(1:floor(m/2)),Power{i}(1:floor(m/2)),C{i})
        hold on;
        %Add labels to the axes
        xlabel('Frequency [Hz]')
        ylabel('Amplitude [mG]')
        %Ade title  to the graph
        title(TitleLabel{i})
        %Look for the index of the highest power in order to display it as a
        %red dot in its corresponding position on top of the previous graph
        index = find(Power{i}(1:floor(m/2)) == max(Power{i}(1:floor(m/2))));
        mainFreqStr = num2str(freq(index));
        string = sprintf('Frequency = %s Hz',mainFreqStr);
        text(freq(index)+0.02,Power{i}(index),string); 
        h(i)= plot(freq(index),Power{i}(index),'r.', 'MarkerSize',25);

        i=i+1;
    end
    hold off;

    mkdir(FolderName);
    fileattrib (FolderName, '+w', '', 's')
    cd(FolderName);

    for i = [1:3]
        saveas(h(i),FileName{i},'bmp') 
        i= i+1;
    end


    %%Plot together
    %Create new window
    figure;
    for i = [1:3]
        %Graph the first half of the data set (second half is redundant)
        plot(freq(1:floor(m/2)),Power{i}(1:floor(m/2)),C{i})
        hold on;
        %Add labels to the axes
        xlabel('cycles/second [Hz]')
        ylabel('Amplitude [mG]')
        %Ade title  to the graph
        title('Full spectral response')
        %Set legend displaying the axes and their colors
        hleg=legend('X Axis','Y Axis','Z Axis');
        set(hleg,'Location','NorthEast');
        i=i+1;
    end
    for i = [1:3]
        %Look for the index of the highest power in order to display it as a
        %red dot in its corresponding position on top of the previous graph
        index = find(Power{i}(1:floor(m/2)) == max(Power{i}(1:floor(m/2))));
        mainFreqStr = num2str(freq(index));
        string = sprintf('Frequency = %s Hz',mainFreqStr);
        text(freq(index)+0.02,Power{i}(index),string);  
        h=plot(freq(index),Power{i}(index),'r.', 'MarkerSize',25);
        FreqPow{i}(1,1)= freq(index);
        FreqPow{i}(1,2)=Power{i}(index);
        i=i+1;
    end
    saveas(h,FolderName,'bmp')
    hold off;
    cd ..
    d = {FolderName,FreqPow{1}(1,1),FreqPow{1}(1,2),FreqPow{2}(1,1),FreqPow{2}(1,2),FreqPow{3}(1,1),FreqPow{3}(1,2)};
    xlswrite('Comparison.xls', d, 1, Cells{z});

z=z+1;
end




disp('Finished');




