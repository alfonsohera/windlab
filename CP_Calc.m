%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Definitions%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Time
Time=[0:1:60];
%Radius [m]
Radius=0.1;
%Moment of inertia of the motor [kg*m^2]
J=0.000366;
%Air density [kg/m^3] Calculated at 24°C and 100.8kPa
rho = 1.17;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Data extraction%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data= xlsread('datos.xls','A1:A62')
Size= size(data);
Rows= Size(1,1);%RPM values
Columns = Size(1,2);%Different wind speeds
C = {'k','b','r','g','y', 'm','c'};%Color array used with plots

%Save data in individual arrays
for i = [1:Columns] 
    WindSpeeds(1,i) = data (1,i);
    for j =[1:Rows-1] 
    RotSpeeds{i}(1,j) = data(j+1,i);
    RotSpeeds{i}(1,j) = RotSpeeds{i}(1,j).*0.1047;%Change from RPMs to rad/s
    j=j+1;
    end
    i=i+1;
end

%Smooth input data
for i = [1:Columns]
    RotSpeeds{i}=smooth(RotSpeeds{i},6);
    RotSpeeds{i}=transpose(RotSpeeds{i});
    i=i+1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Calculations%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = [1:Columns] 
    
    for j =[1:Rows-1] 
    %Tipspeed ratio
    Lambda{i}(1,j) = RotSpeeds{i}(1,j).*Radius/WindSpeeds(1,i);
    j=j+1;
    end
    %Moment M = J*dw/dt
    M{i}=J.*(diff(RotSpeeds{i}));
    M{i}=horzcat(0,M{i});
    %Power P = w*M
    P{i}= RotSpeeds{i}.*M{i};
    %Cp
    Cp{i}=(P{i}./((rho/2)*WindSpeeds(1,i)^3*pi()*Radius^2));
    %Cm
    Cm{i}=(M{i}./((rho/2)*WindSpeeds(1,i)^2*pi()*Radius^3));
    i=i+1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Graphs%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
for i = [1:Columns] 
    plot(Time,RotSpeeds{i},C{i});
    hold on;
    grid on;
    title('Time Vs Rotational Speed');
    xlabel('Time[s]');
    ylabel('Rotational speed [rad/s]');
    i=i+1;
end
hleg=legend('5 m/s','6 m/s','7 m/s','8 m/s','9 m/s','13 m/s','14 m/s');
set(hleg,'Location','SouthEast');
hold off;

figure;
for i = [1:Columns] 
    plot(Time,M{i},C{i});
    hold on;
    grid on;
    title('Time Vs Momentum');
    xlabel('Time[s]');
    ylabel('Momentum [Nm]');
    i=i+1;
end
hleg=legend('5 m/s','6 m/s','7 m/s','8 m/s','9 m/s','13 m/s','14 m/s');
set(hleg,'Location','NorthEast');
hold off;
figure;
for i = [1:Columns]
    plot (Time,P{i},C{i});
    hold on;
    grid on;
    title('Time Vs Power');
    xlabel('Time [s]');
    ylabel('Power [W]');
    i=i+1;
end
hleg=legend('5 m/s','6 m/s','7 m/s','8 m/s','9 m/s','13 m/s','14 m/s');
set(hleg,'Location','NorthEast');
hold off;
figure;
for i = [1:Columns] 
    
    plot(Lambda{i},Cp{i},C{i});
    hold on;
    grid on;
    title('TipSpeed ratio Vs Cp');
    xlabel('Tip Speed Ratio');
    ylabel('Cp');
    i=i+1;
end
hleg=legend('5 m/s','6 m/s','7 m/s','8 m/s','9 m/s','13 m/s','14 m/s');
set(hleg,'Location','NorthWest');
hold off;
figure;
for i = [1:Columns] 
    
    plot(Lambda{i},Cm{i},C{i});
    hold on;
    grid on;
    title('TipSpeed ratio Vs Cm');
    xlabel('Tip Speed Ratio');
    ylabel('Cm');
    i=i+1;
end
hleg=legend('5 m/s','6 m/s','7 m/s','8 m/s','9 m/s','13 m/s','14 m/s');
set(hleg,'Location','NorthEast');
hold off;


