%%BEM Algorithm
DeltaA=1;
Tolerance = 0.001;
ac= 0.2; %Critical induction factor

for i = [1:BladeSegments]
    %Relaxation factor to improve convergence
    RF = 0.5;
    %Initialize Induction factors
    a_temp=0;
    a=0;
    ap=0;
    MaxIter = 1000;
    HasConverged = 0;
    iter=1;
    while ( (HasConverged == 0) && (iter <= MaxIter) )
        %Calculate inflow angle [Rads]
        InflowAngle = atan(((1-a)*DesignWindSpeed)/((1+ap)*DesignOmegaRad*BladeSegmentRadius(i)));
        inflow_segment(i)=InflowAngle;
        %Calculate angle of attack
        AngleOfAttackRad = InflowAngle - BladeTwistRad(i);
        AngleOfAttack = AngleOfAttackRad*180/pi;
        %Look for the values of Cl & Cd that correspond to the found angle of
        %attack
        AngleOfAttack= roundn(AngleOfAttack,-1);
        RowAA= find ((xf.Polars{1,1}.Alpha)==AngleOfAttack);
        %The next if clause is a workaround to avoid errors when looking
        %for AoA not included in the polars *****Needs to be improved*****
        if (isempty(RowAA))
            LiftCoefficient= xf.Polars{1,1}.CL(PolarSize(1,1));
            cl_segment(i) = LiftCoefficient;
            DragCoefficient= xf.Polars{1,1}.CD(PolarSize(1,1));
            cd_segment(i) = DragCoefficient;
        else
            LiftCoefficient= xf.Polars{1,1}.CL(RowAA);
            cl_segment(i) = LiftCoefficient;
            DragCoefficient= xf.Polars{1,1}.CD(RowAA);
            cd_segment(i) = DragCoefficient;
        end
        
        %Calculate Cn & Ct
        Cn= LiftCoefficient*cos(InflowAngle) + DragCoefficient*sin(InflowAngle);
        Ct= LiftCoefficient*sin(InflowAngle) - DragCoefficient*cos(InflowAngle);
        %Calculate the induction factors a & ap with prandtl tip losses
        f = (NumberOfBlades/2)*((RotorRadius-BladeSegmentRadius(i))/(BladeSegmentRadius(i)*sin(InflowAngle)));
        F = (2/pi)*(acos(exp(-1*f)));
        F_segment(i) = F; 
        Solidity(i) = NumberOfBlades*ChordLength(i)/(2*pi*BladeSegmentRadius(i));
        K = 4*F*((sin(InflowAngle))^2)/(Solidity(i)*Cn);
        ap= 1/(((4*F*(sin(InflowAngle)*cos(InflowAngle))/(Solidity(i)*Ct)))-1);
        a=1/(K+1);
        %Check if a is below a critical
        if(a>=ac)
        a=0.5*(2+(K*(1-2*ac))-sqrt(((K*(1-2*ac)+2)^2)+(4*((K*ac^2)-1))));
        end
        %relaxation factor to speed up convergence.
        a = RF*a + (1-RF)*a_temp;
        Delta = a-a_temp;
        %check for convergence
        if (abs(Delta)<Tolerance)
            HasConverged = 1;
            a_segment(i)=a;
            ap_segment(i)=ap;
            %The torque calculation seems to be wrong, the calculated
            %values are too high.
            Thrust(i)=4*F*pi*BladeSegmentRadius(i)*Rho*(DesignWindSpeed^2)*a*(1-a);
            Torque(i)=4*F*pi*(BladeSegmentRadius(i)^3)*Rho*DesignWindSpeed*DesignOmegaRad*(1-a)*ap;
             
        end
        a_temp = a;
        iter = iter +1;
        if (iter > MaxIter)
            text = sprintf('Segmento %g no convergió, delta: %g',i,Delta);
            disp(text);
            disp('---------------------------------------');
            a_segment(i)=a;
            ap_segment(i)=ap;
            Thrust(i)=4*F*pi*BladeSegmentRadius(i)*Rho*(DesignWindSpeed^2)*a*(1-a);
            Torque(i)=4*F*pi*(BladeSegmentRadius(i)^3)*Rho*DesignWindSpeed*DesignOmegaRad*(1-a)*ap;
                       
        end
                      
    end
    %proceed to next segment
    
    i=i+1;
end
 
%Calculate Cp
%Cp = 4.*a_segment.*((1.-a_segment)).^2;
figure;
plot(BladeSegmentRadius,a_segment,'o');
hold on;
grid on;
title('Axial induction factor vs local radius');
xlabel('Local radius[m]');
ylabel('Induction factor');
hold off;



